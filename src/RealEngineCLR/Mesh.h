#pragma once

// External
#pragma managed(push, off)
#include "..\RealEngineCore\CoreMesh.h"
#pragma managed(pop)

// Internal
using namespace System;

public ref class Mesh {
public:
	Mesh(Defacto::Graphics::Mesh::Mesh mesh);
private:
	CoreMesh* coreMesh;
};