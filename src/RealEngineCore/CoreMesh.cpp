#pragma once

#include "CoreMesh.h"

CoreMesh::CoreMesh() {

}

void CoreMesh::SetIndices(int* pos, int size) {
	Indices.clear();
	for (int i = 0; i < size; i++) {
		Indices.push_back(pos[i]);
	}
}

void CoreMesh::SetVertices(float* pos, int size) {
	Vertices.clear();
	for (int i = 0; i < size / 3; i++) {
		Vertex v;
		v.pos.x = pos[i * 3];
		v.pos.y = pos[i * 3 + 1];
		v.pos.z = pos[i * 3 + 2];
		v.color = { 0.5f, 0.5f, 1.0f };
		v.texCoord = { 0.5f, 0.5f };
		Vertices.push_back(v);
	}
}

void CoreMesh::SetUVs(float* pos, int size) {
	Vertices.clear();
	for (int i = 0; i < size / 2; i++) {
		Vertex v;
		v.pos.x = pos[i * 3];
		v.pos.y = pos[i * 3 + 1];
		v.pos.z = pos[i * 3 + 2];
		v.color = { 0.5f, 0.5f, 1.0f };
		v.texCoord = { 0.5f, 0.5f };
		Vertices.push_back(v);
	}
}