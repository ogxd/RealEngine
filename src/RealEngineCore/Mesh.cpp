#pragma once

#include "Mesh.h"
//#include "CoreMesh.cpp"

Mesh::Mesh() {
	coreMesh = new CoreMesh();
}

void Mesh::SetIndices(int* pos, int size) {
	coreMesh->SetIndices(pos, size);
}

void Mesh::SetVertices(float* pos, int size) {
	coreMesh->SetVertices(pos, size);
}

void Mesh::SetUVs(float* pos, int size) {
	coreMesh->SetUVs(pos, size);
}