﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using OGXD.RealEngine.Geometry;

namespace OGXD.RealEngine {

    public static class RealEngineCore {

        public delegate void StringHandler(string value);
        public delegate void VoidHandler();

        public static event VoidHandler Awaked;
        public static event VoidHandler Started;
        public static event VoidHandler Updated;
        public static event StringHandler MessageReceived;

        public static void InvokeAwaked() {
            Awaked?.Invoke();
        }
        public static void InvokeStarted() {
            Started?.Invoke();
        }
        public static void InvokeUpdated() {
            Updated?.Invoke();
        }
        public static void InvokeMessageReceived(string text) {
            MessageReceived?.Invoke(text);
        }

        private static StringHandler _MessageHandler = ConsoleWriteLine;
        public static StringHandler MessageHandler {
            get { return _MessageHandler; }
            set { _MessageHandler = value; }
        }

        public static void WriteLine(string message) {
            _MessageHandler.Invoke(message);
        }

        private static void ConsoleWriteLine(string message) {
            Console.WriteLine("[CORE] " + message);
        }
    }
}
