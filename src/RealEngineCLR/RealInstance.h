#pragma once

// External
//#pragma managed(push, off)
#include "..\RealEngineCore\CoreRealInstance.h"
//#pragma managed(pop)

// Internal
#include "RealObject.h"
#include "Mesh.h"

using namespace System;

public ref class RealInstance {
public:
	RealInstance();
	void Run();
	void Add(Mesh^ realObject);
private:
	CoreRealInstance* coreRealInstance;
};