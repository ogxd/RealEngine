﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace OGXD.RealEngine {

    public class RealEngine {

        public delegate void StringHandler(string value);

        [DllImport(@"RealEngineCore.dll", CallingConvention = CallingConvention.Cdecl)]
        private extern static int run();

        public RealEngine() {
            RealEngineCore.MessageReceived += (message) => RealEngineCore.WriteLine(message);
        }

        public void start() {
            run();
        }
    }
}
