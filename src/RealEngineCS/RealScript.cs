﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OGXD.RealEngine {
    public abstract class RealScript {

        public RealScript() {
            RealEngineCore.Awaked += awake;
            RealEngineCore.Started += start;
            RealEngineCore.Updated += update;

            Geometry.Vertex;
        }

        public virtual void start() { }

        public virtual void awake() { }

        public virtual void update() { }
    }
}
